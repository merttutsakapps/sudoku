package com.example.mert.sudoku.interfaces.Dialog.Internet;

import com.example.mert.sudoku.fragments.BaseFragment;

public interface OnRespondListener {
    void onChoice(String choice, BaseFragment fragment);
}
