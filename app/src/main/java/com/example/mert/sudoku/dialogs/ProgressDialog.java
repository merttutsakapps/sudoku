package com.example.mert.sudoku.dialogs;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mert.sudoku.R;
import com.example.mert.sudoku.activities.BaseActivity;

import dmax.dialog.SpotsDialog;

public class ProgressDialog extends SpotsDialog {

    private BaseActivity baseActivity;

    public ProgressDialog(@NonNull BaseActivity baseActivity) {
        super(baseActivity);
        this.baseActivity = baseActivity;
    }

    @Override
    public void create() {
        super.create();
        /*this.setContentView(R.layout.dialog_progres);

        final ProgressBar progressBar = (ProgressBar) this.findViewById(R.id.progressBar);

        this.setCancelable(false);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        progressBar.setActivated(true)*/

        if (!this.isShowing()) {
            this.show();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }
}


