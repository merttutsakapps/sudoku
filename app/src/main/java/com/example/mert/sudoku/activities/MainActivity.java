package com.example.mert.sudoku.activities;

import android.os.Bundle;

import com.example.mert.sudoku.fragments.LoginFragment;
import com.example.mert.sudoku.fragments.MainFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            initView(new MainFragment());
    }
}
