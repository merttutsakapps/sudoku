package com.example.mert.sudoku.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mert.sudoku.R;
import com.example.mert.sudoku.dialogs.SelectionPhotoDialog;
import com.example.mert.sudoku.interfaces.FireBase.User.OnCreateCallback;
import com.example.mert.sudoku.interfaces.Photo.OnChangePhotoListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignUpFragment extends BaseFragment implements OnCreateCallback {

    //EditText
    private EditText editTextUserName;
    private EditText editTextPassword;
    private EditText[] editText;

    //Button
    private Button buttonSignUp;

    //TextView
    private TextView textViewForgetPassword;
    private TextView textViewSignUp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        setPageName("SignUpFragment");

        //EditText
        editTextUserName = (EditText) view.findViewById(R.id.editText_user_name);
        editTextPassword = (EditText) view.findViewById(R.id.editText_password);

        editText = new EditText[]{
                editTextUserName,
                editTextPassword
        };


        //Button
        buttonSignUp = (Button) view.findViewById(R.id.button_sign_up);

        //Transactions
        userTransaction.setOnCreateCallback(SignUpFragment.this);

        //Button
        buttonSignUp = (Button) view.findViewById(R.id.button_sign_up);


        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ControlContent()) {
                    userTransaction.createUser(
                            editTextUserName.getText().toString(),
                            editTextPassword.getText().toString());
                }
            }
        });
        return view;
    }


    private boolean ControlContent() {
        if (editText[0].getText().length() == 0 && editText[1].getText().length() == 0) {
            Log.d("LOGIN", "Username and password is empty.");

            editText[0].setText("");
            editText[1].setText("");

            editText[0].setError("Kullanıcı adı giriniz.");
            editText[1].setError("Şifre giriniz.");

            Toast.makeText(baseActivity, "Kullanıcı adınızı ve şifrenizi giriniz !", Toast.LENGTH_LONG).show();
            return false;
        } else if (editText[0].getText().length() == 0) {
            Log.d("LOGIN", "Username is empty.");

            editText[0].setText("");
            editText[1].setText("");

            editText[0].setError("Kullanıcı adı giriniz.");
            editText[1].setError("Şifre giriniz.");

            Toast.makeText(baseActivity, "Kullanıcı adınızı giriniz", Toast.LENGTH_LONG).show();
            return false;
        } else if (editText[1].getText().length() == 0) {
            Log.d("LOGIN", "Password is empty.");

            editText[1].setText("");

            editText[1].setError("Şifre giriniz.");

            Toast.makeText(baseActivity, "Şifrenizi giriniz !", Toast.LENGTH_LONG).show();
            return false;
        }

        Log.d("LOGIN", "Content : true");
        return true;
    }

    @Override
    public void onSuccess() {
        editText[0].setText("");
        editText[1].setText("");

        replaceFragment(new GameFragment());
    }

    @Override
    public void onError() {

    }
}