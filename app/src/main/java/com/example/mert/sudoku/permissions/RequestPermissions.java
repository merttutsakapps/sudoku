package com.example.mert.sudoku.permissions;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.example.mert.sudoku.activities.BaseActivity;
import com.example.mert.sudoku.activities.SplashActivity;

public class RequestPermissions {
    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    private Activity activity;

    public RequestPermissions(SplashActivity splashActivity) {

        this.activity = splashActivity;
    }

    public RequestPermissions(BaseActivity baseActivity) {

        this.activity = baseActivity;
    }

    public boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(activity,
                String.valueOf(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}));
        Log.d("CHECKPERMISSIONS", String.valueOf(PackageManager.PERMISSION_GRANTED));
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermissions() {
        boolean shouldProvideRationaleGPS =
                ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.ACCESS_FINE_LOCATION);
        boolean shouldProvideRationaleCAMERA =
                ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.CAMERA);
        boolean shouldProvideRationaleFILE =
                ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);
        boolean shouldProvideRationale = false;

        if (shouldProvideRationaleCAMERA && shouldProvideRationaleFILE && shouldProvideRationaleGPS) {
            shouldProvideRationale = true;
        } else {
        }

        Log.d("REQUEST", "Requesting permission");
        startRequestPermission();
        //}
    }

    public void startRequestPermission() {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }
}
