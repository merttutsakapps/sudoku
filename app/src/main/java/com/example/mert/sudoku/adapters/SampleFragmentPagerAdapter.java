package com.example.mert.sudoku.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.mert.sudoku.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
    private List<BaseFragment> views;

    private String tabTitles[] = new String[]{"GİRİŞ YAP", "KAYIT OL"};

    public SampleFragmentPagerAdapter(FragmentManager fm, ArrayList<BaseFragment> fragments) {
        super(fm);
        views = fragments;
        saveState();
    }
    
    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public Fragment getItem(int position) {
        return views.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }


}