package com.example.mert.sudoku.activities;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mert.sudoku.R;
import com.example.mert.sudoku.dialogs.InternetDialog;
import com.example.mert.sudoku.fragments.BaseFragment;
import com.example.mert.sudoku.interfaces.Photo.OnChangePhotoListener;
import com.example.mert.sudoku.interfaces.Dialog.Internet.OnRespondListener;
import com.example.mert.sudoku.services.ControlInternetServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.view.View.VISIBLE;

public class BaseActivity extends FragmentActivity implements OnRespondListener {
    //Profil Photo
    public static final int IMAGE_CAPTURE = 1;
    public static final int IMAGE_PICK = 2;

    //FireBase
    public static final String FIREBASE_URL = "https://sudoku-463fb.firebaseio.com/";
    public FirebaseDatabase firebaseDb;
    public FirebaseAuth firebaseAuth;

    //ActionBar
    private ImageButton headerLeft;
    private ImageButton headerRight;
    private TextView headerMid;

    //FrameLayout
    private FrameLayout frameLayout;

    //Dialog
    private InternetDialog internetDialog;

    //Bundle
    private Bundle bundle;

    //Profil Photo
    private OnChangePhotoListener changePhotoListener;

    //Services
    public ControlInternetServices controlInternetServices;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        //Services
        controlInternetServices = new ControlInternetServices(BaseActivity.this);
        firebaseDb = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        //Bundle
        bundle = new Bundle();

        //Dialog

        //ActionBar
        headerLeft = (ImageButton) findViewById(R.id.header_left);
        headerRight = (ImageButton) findViewById(R.id.header_right);
        headerMid = (TextView) findViewById(R.id.header_text);

        headerLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        headerRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //changeFragment(new ProfilUpdateFragment());
            }
        });

        //FrameLayout
        frameLayout = (FrameLayout) findViewById(R.id.main_framelayout);
    }


    public void setTextHeadermid(String text) {
        if (text != null) {
            headerMid.setText(text.toUpperCase());
        }
    }

    public void setVisibilityHeaderButton(boolean left, boolean right) {

        if (left == true) {
            headerLeft.setVisibility(VISIBLE);
        } else {
            headerLeft.setVisibility(View.GONE);
        }

        if (right == true) {
            headerRight.setVisibility(VISIBLE);
        } else {
            headerRight.setVisibility(View.GONE);
        }
    }

    public void initView(final BaseFragment fragment) {
        if (accessOperation("INIT", fragment)) {
            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.to_right_animation, R.anim.from_left_animation, R.anim.from_right_animation, R.anim.to_left_animation).replace(R.id.main_framelayout, fragment).commit();
        }
    }

    public void changeFragment(BaseFragment fragment) {
        if (accessOperation("CHANGE", fragment)) {
            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.to_right_animation, R.anim.from_left_animation, R.anim.from_right_animation, R.anim.to_left_animation).replace(R.id.main_framelayout, fragment).addToBackStack(fragment.toString()).commit();
            Log.d("STACK", "PUSH => " + fragment.toString());
        }
    }

    @Override
    public void onBackPressed() {
        try {
            if (accessOperation("BACK", null)) {
                Log.d("STACK", "POP => " + getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1));
            }
        } catch (Exception e) {
            Log.e("STACK", "error :" + e.getMessage().toString());
        }
        super.onBackPressed();
    }

    private boolean accessOperation(String choice, BaseFragment fragment) {
        internetDialog = new InternetDialog(this, choice, fragment);
        internetDialog.setOnRespondListener(BaseActivity.this);

        if (!controlInternetServices.getInfoInternet()) {
            internetDialog.create();
            return false;
        } else {
            return true;
        }
    }

    public void setArguments(Bundle bundle) {
        Log.d("BUNDLE", "setArguments : " + bundle.toString());
        this.bundle.putAll(bundle);
    }

    public Bundle getArguments() {
        Log.d("BUNDLE", "getArguments : " + this.bundle.toString());
        return this.bundle;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_CAPTURE) {
                Log.d("CAMERA", "VERİ GELDİ");
                if (getArguments().getString("profilImagePath", "") != "") {
                    String path = getArguments().getString("profilImagePath");
                    changePhotoListener.ChangePhoto(path);
                }
            } else if (requestCode == IMAGE_PICK) {
                Log.d("GALLERY", "VERİ GELDİ");

                if (data != null) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String path = cursor.getString(columnIndex);
                    changePhotoListener.ChangePhoto(path);
                    cursor.close();
                }
            }
        } else if (requestCode == RESULT_CANCELED) {
            if (requestCode == IMAGE_CAPTURE) {
                Log.d("CAMERA", "VERİ GELDİ. CANCELLED");
                if (getArguments().getString("profilImagePath", "") != "") {
                    new File(getArguments().getString("profilImagePath")).delete();
                }
            }
        } else if (requestCode == IMAGE_PICK) {
            Log.d("GALLERY", "VERİ GELDİ");
        }
    }

    public File createImageFile() throws IOException {
        final File imageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + BaseActivity.this.getResources().getString(R.string.app_name));

        if (!imageFile.exists()) {
            imageFile.mkdirs();
        }
        // Create an image file name
        String imageFileName = "JPEG_" + new SimpleDateFormat("yyyyMMdd_HHmm").format(new Date()) + "_";

        File storageDir = imageFile;

        File image = File.createTempFile(
                imageFileName,  // prefix
                ".png",         // suffix
                storageDir      // directory
        );

        return image;
    }

    public void setChangePhotoListener(OnChangePhotoListener changePhotoListener) {
        this.changePhotoListener = changePhotoListener;
    }

    @Override
    public void onChoice(String choice, BaseFragment fragment) {
        Log.i("INTERNET", "info : " + controlInternetServices.getInfoInternet());

        if (controlInternetServices.getInfoInternet()) {
            Toast.makeText(this, "Thank you for continuing with us :)", Toast.LENGTH_SHORT).show();
            if (choice == "CHANGE") {
                Log.i("FRAGMENT", fragment.toString());
                if (fragment != null) {
                    changeFragment(fragment);
                }
            } else if (choice == "INIT") {
                Log.i("FRAGMENT", fragment.toString());
                if (fragment != null) {
                    initView(fragment);
                }
            } else if (choice == "BACK") {
                Log.i("FRAGMENT", fragment.toString());
                onBackPressed();
            }
        } else if (!controlInternetServices.getInfoInternet()) {
            if (!controlInternetServices.getInfoInternet()) {
                internetDialog = new InternetDialog(this, choice, fragment);
                internetDialog.setOnRespondListener(BaseActivity.this);
                internetDialog.create();
            }
        }
    }
}
