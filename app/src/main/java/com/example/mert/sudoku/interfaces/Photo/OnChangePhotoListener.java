package com.example.mert.sudoku.interfaces.Photo;

import android.support.annotation.Nullable;

/**
 * Created by mert on 20/07/2017.
 */

public interface OnChangePhotoListener {
    void ChangePhoto(@Nullable String path);
}
