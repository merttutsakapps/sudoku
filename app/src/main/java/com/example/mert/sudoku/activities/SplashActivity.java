package com.example.mert.sudoku.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mert.sudoku.R;

public class SplashActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageView = (ImageView) findViewById(R.id.imageView_splash_logo);
        textView = (TextView) findViewById(R.id.textView_splash);

        Animation animation_logo = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        Animation animation_text = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        animation_logo.setDuration(2000);
        animation_text.setDuration(1000);

        imageView.startAnimation(animation_logo);
        textView.startAnimation(animation_text);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                SplashActivity.this.finish();
            }
        }, 3500);
    }
}
