package com.example.mert.sudoku.firebases.Game.model;

import android.graphics.Matrix;


public class Game {

    private int gameId;
    private int score;
    private long time;
    private Matrix matrix;

    public int getGameId() {
        return gameId;
    }

    private void setGameId(int _gameId) {
        this.gameId = _gameId;
    }

    public int getScore() {
        return score;
    }

    private void setScore(int _score) {
        this.score = _score;
    }

    public long getTime() {
        return time;
    }

    private void setTime(long _time) {
        this.time = _time;
    }

    public Matrix getMatrix() {
        return matrix;
    }

    public void setMatrix(Matrix _matrix) {
        this.matrix = _matrix;
    }

    public boolean setGame(int _gameId, int _score, long _time, Matrix _matrix) {
        try {
            this.gameId = _gameId;
            this.score = _score;
            this.time = _time;
            this.matrix = _matrix;

            return true;
        } catch (Exception e) {

            return false;
        }
    }

    @Override
    public String toString() {
        return "Game{" +
                "gameId=" + gameId +
                ", score=" + score +
                ", time=" + time +
                ", matrix=" + matrix +
                '}';
    }
}
