package com.example.mert.sudoku.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.mert.sudoku.R;
import com.example.mert.sudoku.activities.BaseActivity;
import com.example.mert.sudoku.fragments.BaseFragment;
import com.example.mert.sudoku.interfaces.Dialog.Internet.OnRespondListener;

public class InternetDialog extends Dialog {
    //Activity
    private BaseActivity baseActivity;
    //Listeners
    private OnRespondListener onRespondListener;
    //Button
    private Button buttonOkey;
    private Button buttonClose;
    //String
    private String choice;
    //Fragment
    private BaseFragment fragment;

    public InternetDialog(@NonNull Context context, String choice, BaseFragment fragment) {
        super(context);
        this.baseActivity = (BaseActivity) context;
        this.choice = choice;
        this.fragment = fragment;
    }

    @Override
    public void create() {
        super.create();
        this.setContentView(R.layout.dialog_internet);
        this.setCancelable(false);

        buttonOkey = (Button) this.findViewById(R.id.button_internet_control);
        buttonClose = (Button) this.findViewById(R.id.button_app_close);

        buttonOkey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!baseActivity.controlInternetServices.getInfoInternet()) {
                    Toast.makeText(baseActivity, "Lütfen tekrar deneyiniz!", Toast.LENGTH_SHORT).show();
                } else {
                    onRespondListener.onChoice(choice, fragment);
                    dismiss();
                }
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.finish();
            }
        });

        if (!isShowing()) {
            show();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void show() {
        super.show();
    }

    public void setOnRespondListener(OnRespondListener onRespondListener) {
        Log.d(onRespondListener.getClass().getSimpleName(), onRespondListener != null ? "onRespondListener null degil" : "-----null-------");
        this.onRespondListener = onRespondListener;
    }
}