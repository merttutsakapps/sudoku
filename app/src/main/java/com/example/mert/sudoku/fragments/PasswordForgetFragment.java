package com.example.mert.sudoku.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mert.sudoku.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PasswordForgetFragment extends BaseFragment {


    public PasswordForgetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_password_forget, container, false);

        setPageName("PasswordForgetFragment");

        return view;
    }

}
