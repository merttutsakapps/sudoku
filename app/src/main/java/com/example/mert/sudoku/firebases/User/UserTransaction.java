package com.example.mert.sudoku.firebases.User;

import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.mert.sudoku.R;
import com.example.mert.sudoku.activities.BaseActivity;
import com.example.mert.sudoku.dialogs.ProgressDialog;
import com.example.mert.sudoku.firebases.User.model.User;
import com.example.mert.sudoku.interfaces.FireBase.User.*;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UserTransaction {
    private static final int RC_SIGN_IN = 9001;

    //FireBase
    private FirebaseDatabase firebaseDb;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference dbRef;

    //Activity
    private BaseActivity baseActivity;

    //Callbacks
    private OnCreateCallback onCreateCallback;
    private OnLoginCallback onLoginCallback;

    //Client
    private GoogleApiClient mGoogleApiClient;

    //Dialogs
    protected ProgressDialog progressDialog;

    public UserTransaction(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
        //database erişim
        this.firebaseDb = baseActivity.firebaseDb;
        //databaseRef
        dbRef = firebaseDb.getReference("users");
        //auth
        firebaseAuth = baseActivity.firebaseAuth;
        //progress dialog
        progressDialog = new ProgressDialog(baseActivity);
    }

    public void createUser(String _email, String _password) {
        User user = new User();
        user.setUserName(_email);
        user.setPassword(_password);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressDialog.create();
        }
        firebaseAuth.createUserWithEmailAndPassword(user.getUserName(), user.getPassword()).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                onCreateCallback.onSuccess();
                Log.d("USER_TRANSACTION", "onCreateCallback -> succesful");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onCreateCallback.onError();
                Toast.makeText(baseActivity, e.getMessage(), Toast.LENGTH_LONG).show();
                Log.d("USER_TRANSACTION", "onCreateCallback -> failure");
            }
        }).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressDialog.dismiss();
            }
        });
    }

    /*
    //Kullanıcı sayısı
    public int Count() {

        return 0;
    }

    private boolean hasUserName(final String _userName) {
        Log.d("USER_TRANSACTION", "hasUserName -> parameter user name :" + _userName);
        return true;
    }

    private boolean isCorrectPassword(final String _userName, final String _password) {
       return true;
    }
*/
    public void loginUser(String _email, String _password) {
        firebaseAuth.signInWithEmailAndPassword(_email, _password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                onLoginCallback.onSuccess();
                Log.d("USER_TRANSACTION", "onLoginCallback -> succesful");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                onLoginCallback.onError();
                Toast.makeText(baseActivity, e.getMessage(), Toast.LENGTH_LONG).show();
                Log.d("USER_TRANSACTION", "onLoginCallback -> failure");
            }
        }).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressDialog.dismiss();
            }
        });
    }

    public void setOnCreateCallback(OnCreateCallback onCreateCallback) {
        Log.d(onCreateCallback.getClass().getSimpleName(), onCreateCallback != null ? "onRespondListener null degil" : "-----null-------");
        this.onCreateCallback = onCreateCallback;
    }

    public void setOnLoginCallback(OnLoginCallback onLoginCallback) {
        Log.d(onLoginCallback.getClass().getSimpleName(), onLoginCallback != null ? "onRespondListener null degil" : "-----null-------");
        this.onLoginCallback = onLoginCallback;
    }
}
