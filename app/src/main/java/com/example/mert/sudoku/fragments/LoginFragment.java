package com.example.mert.sudoku.fragments;


import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mert.sudoku.R;
import com.example.mert.sudoku.interfaces.FireBase.User.OnCreateCallback;
import com.example.mert.sudoku.interfaces.FireBase.User.OnLoginCallback;

public class LoginFragment extends BaseFragment implements OnLoginCallback {

    //EditText
    private EditText editTextUserName;
    private EditText editTextPassword;
    private EditText[] editText;

    //Checkbox
    private CheckBox checkBoxRememberMe;

    //Button
    private Button buttonLogin;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        setPageName("LoginFragment");

        //EditText
        editTextUserName = (EditText) view.findViewById(R.id.editText_user_name);
        editTextPassword = (EditText) view.findViewById(R.id.editText_password);

        editText = new EditText[]{
                editTextUserName,
                editTextPassword
        };

        //Checkbox
        checkBoxRememberMe = (CheckBox) view.findViewById(R.id.checkBox_remember_me);

        //Transactions
        userTransaction.setOnLoginCallback(LoginFragment.this);

        //Button
        buttonLogin = (Button) view.findViewById(R.id.button_login);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ControlContent()) {
                    userTransaction.loginUser(
                            editTextUserName.getText().toString(),
                            editTextPassword.getText().toString());
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("LOGIN_FRAGMENT","onResume");
        if (!sharedPreference.isEmptySession()) {
            editTextUserName.setText(sharedPreference.getUserName());
            editTextPassword.setText(sharedPreference.getPassword());
        } else {
            editTextUserName.setText("");
            editTextPassword.setText("");
        }
        checkBoxRememberMe.setChecked(true);
    }

    private boolean ControlContent() {
        if (editText[0].getText().length() == 0 && editText[1].getText().length() == 0) {
            Log.d("LOGIN", "Username and password is empty.");

            editText[0].setText("");
            editText[1].setText("");

            editText[0].setError("Kullanıcı adı giriniz.");
            editText[1].setError("Şifre giriniz.");

            Toast.makeText(baseActivity, "Kullanıcı adınızı ve şifrenizi giriniz !", Toast.LENGTH_LONG).show();
            return false;
        } else if (editText[0].getText().length() == 0) {
            Log.d("LOGIN", "Username is empty.");

            editText[0].setText("");
            editText[1].setText("");

            editText[0].setError("Kullanıcı adı giriniz.");
            editText[1].setError("Şifre giriniz.");

            Toast.makeText(baseActivity, "Kullanıcı adınızı giriniz", Toast.LENGTH_LONG).show();
            return false;
        } else if (editText[1].getText().length() == 0) {
            Log.d("LOGIN", "Password is empty.");

            editText[1].setText("");

            editText[1].setError("Şifre giriniz.");

            Toast.makeText(baseActivity, "Şifrenizi giriniz !", Toast.LENGTH_LONG).show();
            return false;
        }

        Log.d("LOGIN", "Content : true");
        return true;
    }

    @Override
    public void onSuccess() {
        sharedPreference.setLoginFillIn(
                editTextUserName.getText().toString(),
                editTextPassword.getText().toString(),
                checkBoxRememberMe.isChecked());

        replaceFragment(new GameFragment());
    }

    @Override
    public void onError() {

    }
}
