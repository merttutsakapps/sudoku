package com.example.mert.sudoku.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.mert.sudoku.activities.BaseActivity;


public class SharedPreference {

    private BaseActivity baseActivity;
    private SharedPreferences preferencesProfil;
    private SharedPreferences.Editor editor;

    public SharedPreference(BaseActivity baseActivity) {

        this.baseActivity = baseActivity;

        preferencesProfil = this.baseActivity.getSharedPreferences("profil", Context.MODE_PRIVATE);
        editor = preferencesProfil.edit();
    }

    public void setUserName(String _userName) {
        Log.i("SHARED_PREFERENCES", "set user name :" + _userName);
        editor.putString("USER_NAME", _userName).commit();
    }

    public String getUserName() {
        Log.i("SHARED_PREFERENCES", "get user name :" + preferencesProfil.getString("USER_NAME", "").toString());
        return preferencesProfil.getString("USER_NAME", "");
    }

    public void setPassword(String _password) {
        Log.i("SHARED_PREFERENCES", "set password :" + _password);
        editor.putString("PASSWORD", _password).commit();
    }

    public String getPassword() {
        Log.i("SHARED_PREFERENCES", "get password :" + preferencesProfil.getString("PASSWORD", "").toString());
        return preferencesProfil.getString("PASSWORD", "");
    }

    public void setLoginFillIn(String _userName, String _password, boolean _isChecked) {
        Log.i("SHARED_PREFERENCES", "isChecked :" + _isChecked);
        if (_isChecked) {
            setUserName(_userName);
            setPassword(_password);
        } else {
            setUserName("");
            setPassword("");
        }
    }

    public boolean isEmptySession() {
        if (getUserName() != "" && getPassword() != "") {
            return false;
        } else {
            return true;
        }
    }
}
