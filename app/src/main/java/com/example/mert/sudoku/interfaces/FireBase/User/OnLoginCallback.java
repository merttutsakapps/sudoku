package com.example.mert.sudoku.interfaces.FireBase.User;

/**
 * Created by mert on 03/08/2017.
 */

public interface OnLoginCallback {
    void onSuccess();
    void onError();
}
