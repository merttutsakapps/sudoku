package com.example.mert.sudoku.services;

import android.net.ConnectivityManager;

import com.example.mert.sudoku.activities.BaseActivity;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by mert on 02/08/2017.
 */

public class ControlInternetServices {
    BaseActivity baseActivity;

    public ControlInternetServices(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public boolean getInfoInternet() {
        ConnectivityManager conMgr = (ConnectivityManager) baseActivity.getSystemService(CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
