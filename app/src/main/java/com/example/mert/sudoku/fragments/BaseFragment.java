package com.example.mert.sudoku.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.example.mert.sudoku.R;
import com.example.mert.sudoku.activities.BaseActivity;
import com.example.mert.sudoku.dialogs.ProgressDialog;
import com.example.mert.sudoku.firebases.Game.GameTransaction;
import com.example.mert.sudoku.firebases.User.UserTransaction;
import com.example.mert.sudoku.sharedpreferences.SharedPreference;

public class BaseFragment extends Fragment {
    //Activities
    protected BaseActivity baseActivity;
    //Preferences
    protected SharedPreference sharedPreference;
    //Dialogs
    protected ProgressDialog progressDialog;
    //Transaction
    protected UserTransaction userTransaction;
    protected GameTransaction gameTransaction;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseActivity = (BaseActivity) getActivity();
        sharedPreference = new SharedPreference(baseActivity);

        //Realms
        userTransaction = new UserTransaction(baseActivity);
        gameTransaction= new GameTransaction(baseActivity);

        baseActivity.setTextHeadermid(getResources().getString(R.string.app_name));
        baseActivity.setVisibilityHeaderButton(false, true);
        progressDialog = new ProgressDialog(baseActivity);
    }

    protected void setPageName(String name) {
        Bundle args = new Bundle();
        args.putString("pageName", name);
        Log.i("BUNDLE", "pageName : " + name);
        baseActivity.setArguments(args);
    }

    protected boolean replaceFragment(BaseFragment _fragment) {
        if (_fragment != null) {
            baseActivity.changeFragment(_fragment);
            return true;
        } else {
            return false;
        }
    }

    protected boolean setHeaderTextActionBar(String _text) {
        if (_text != null || _text != "") {
            baseActivity.setTextHeadermid(_text);
            return true;
        } else {
            return false;
        }
    }

    protected boolean setButtonVisibilityActionBar(boolean _left, boolean _right) {
        baseActivity.setVisibilityHeaderButton(_left, _right);
        return true;
    }

    protected boolean backPressButton() {
        baseActivity.onBackPressed();
        return true;
    }
}